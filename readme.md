# AWS Lambda

This is a Terraform module to build :
- Lambda function

Parameters needed :
- **name** : Function name
- **package** : Zip filename
- **role** : Fully qualified name (ARN) of Lambda IAM role
- **handler** : Function handler
- **description** : Description
- **timeout** : Function timeout in seconds (default value : 30)
- **memory** : Lambda memory size in MB (default value : 128)
- **runtime** : Lambda runtime version (default value : python3.9)
- **architecture** : Lambda instruction set architecture (default value : arm64)
- **publish** : Lambda new version to publish (default value : false)
- **retention** : Lambda execution logs retention in days (default value : 30)
- **tags** : Application's tags
- **layers** : Lambda layers (optional : if not present, no layer will be configured)

Output values :
- **name** : Function name
- **dev** : Qualifier of Lambda DEV version
- **prod** : Qualifier of Lambda PROD version
