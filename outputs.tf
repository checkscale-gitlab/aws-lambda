output "name" {
  value       = aws_lambda_function.LambdaFunction.function_name
  description = "Function name"
}

output "dev" {
  value       = aws_lambda_alias.LambdaDev.name
  description = "Qualifier of Lambda DEV version"
}

output "prod" {
  value       = aws_lambda_alias.LambdaProd.name
  description = "Qualifier of Lambda PROD version"
}

output "arn" {
  value       = aws_lambda_function.LambdaFunction.arn
  description = "Function's ARN"
}
